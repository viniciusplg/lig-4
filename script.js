let containerTabela = document.querySelector('#containerTabela') // pega id da tabela
let bolaVermelha = document.createElement('div') // cria a div da bola vermelha
let bolaPreta = document.createElement('div') // cria a div da bola preta
let playerText = document.getElementById('playerText') // pega id do texto do player
let msgWin = document.getElementById('msgWin') // pega id do msg de vitória
const getButton = document.querySelector('#buttonRestart') // pega id do btn de resetar

let corAtual = 1 // contador de bolas nas células
let color = "red"; // valida a movimentação da bola no clique e nas setinhas
let pos = 1 // pega a posição da bolinha que vai ser adicionada na coluna
let posCelulas = 0; // pega a posição da célula após a bolinha ser adicionada na coluna

bolaVermelha.className = 'bolaVermelha' // estilo da bola vermelha
bolaPreta.className = 'bolaPreta' // estilo da bola preta
playerText.className = 'playerText' // estilo do texto do player

// Função para criar a bola
const createBall = (element, ball) => {
    // adiciona a bola ao elemento
    element.appendChild(ball)
}

// Cria a tabela com as colunas
for (let i = 1; i < 8; i++) {
    const mkCol = document.createElement('div') // cria div para coluna
    mkCol.id = i // atribui i ao id da coluna
    mkCol.className = 'colunas' // estilo das colunas
    containerTabela.appendChild(mkCol) // adiciona a coluna na tabela
    for (let k = 1; k < 7; k++) {
        const mkCel = document.createElement('div') // cria as células da coluna
        mkCel.className = `${k} celulas`
        mkCol.appendChild(mkCel) // adiciona a célula na coluna
    }
}

// Cria a linha para a movimentação da bola
for (let i = 1; i < 8; i++) {
    const mkCol = document.createElement('div')
    mkCol.className = `${i} posBolasSuperior`
    posBolas.appendChild(mkCol)

    // Cria a bolinha na primeira coluna

    if (Number(mkCol.className[0]) === 1) {
        createBall(mkCol, bolaVermelha)
    }

    // Movimentação da bola pelo mouse

    mkCol.addEventListener('click', function () {

        // se for igual a red a bola vermelha que irá se movimentar
        if (color === "red") {
            createBall(mkCol, bolaVermelha)
        }

        // se for igual a black a bola preta que irá se movimentar
        if (color === "black") {
            createBall(mkCol, bolaPreta)
        }

        // recebe posição atual da bolinha ao clicar
        pos = Number(mkCol.className[0])

    })

}

// Função para animação
const animaDescida = (coluna, linha) => {
    // Pega o elemento que acabou de descer
    const bola = document.getElementById(coluna).childNodes[linha - 1].firstChild
    // Pega a distância para o topo do elemento, para fazer o ponto final do loop
    const distTop = bola.offsetTop
    let position = 0
    //Coloca o elemento na posição 0, começar a descida
    bola.style.top = `0px`;
    //Função de animação
    let intervalo = setInterval(function () {
        if (position == distTop) {
            clearInterval(intervalo) //Para a animação quando chega na posição
            // position = position - 15
            let quicarBolaUp = setInterval(() => {
                if (position == distTop - 15) {
                    clearInterval(quicarBolaUp)
                    let quicarBolaDown = setInterval(() => {
                        if (position == distTop) {
                            clearInterval(quicarBolaDown)
                        } else {
                            bola.style.top = `${position}px`;
                            position++
                        }
                    }, 10);
                } else {
                    bola.style.top = `${position}px`;
                    position--
                }
            }, 10);

        } else {
            bola.style.top = `${position}px`; //Vai descendo...
            position++
        }
    }, 1)

}

// Movimentação da bola pelo teclado

document.addEventListener('keydown', (event) => {
    const keyName = event.key;

    // Se o usuário apertar a setinha para direita
    if (keyName === "ArrowRight") {

        // posição da bolinha ser menor que 7
        if (pos < 7) {

            // aumenta um na posição 
            pos += 1
            let posInicial = document.getElementsByClassName(`${pos} posBolasSuperior`);

            // se for igual a red a bola vermelha que irá se movimentar
            if (color === "red") {
                createBall(posInicial[0], bolaVermelha)
            }

            // se for igual a black a bola preta que irá se movimentar
            if (color === "black") {
                createBall(posInicial[0], bolaPreta)
            }
        }
    }

    // Se o usuário apertar a setinha para esquerda

    if (keyName === "ArrowLeft") {

        // posição da bolinha ser maior que 1
        if (pos > 1) {

            // diminui um na posição 
            pos -= 1
            let posInicial = document.getElementsByClassName(`${pos} posBolasSuperior`);

            // se for igual a red a bola vermelha que irá se movimentar
            if (color === "red") {
                createBall(posInicial[0], bolaVermelha)
            }

            // se for igual a black a bola preta que irá se movimentar
            if (color === "black") {
                createBall(posInicial[0], bolaPreta)
            }

        }
    }

    // Se o usuário apertar a setinha para baixo
    if (keyName === "ArrowDown") {
        const posTab = document.getElementById(`${pos}`)
        const posAtual = document.getElementsByClassName(`${pos} posBolasSuperior`)

        if (pos == posTab.id) { // A posição atual é igual da tabela
            for (let i of posTab.children) {
                if (i.hasChildNodes() == false) {
                    if (corAtual % 2 == 0) {
                        createBall(i, bolaPreta.cloneNode()) // adiciona a bola na posição da célula
                        posAtual[0].removeChild(bolaPreta) // remove a bola antiga para adicionar a nova
                        createBall(posAtual[0], bolaVermelha)
                        posCelulas = Number(i.className[0]) // pega a posição da célula que foi adicionado a bola
                        color = "red";
                        animaDescida(pos, posCelulas) //função de animação
                        testAllDirections(pos, posCelulas) //aciona a função para verificar se houve vitória
                        playerText.innerHTML = "Player 1" // troca o texto para indicar qual player esta jogando
                        playerText.style.color = "red";
                        corAtual++ //jogado para dentro do loop 
                        break

                    } else if (corAtual % 2 == 1) {
                        createBall(i, bolaVermelha.cloneNode()) // adiciona a bola na posição da célula
                        posAtual[0].removeChild(bolaVermelha) // remove a bola antiga para adicionar a nova
                        createBall(posAtual[0], bolaPreta)
                        posCelulas = Number(i.className[0]) // pega a posição da célula que foi adicionado a bola
                        color = "black";
                        animaDescida(pos, posCelulas) //função de animação
                        testAllDirections(pos, posCelulas) //aciona a função para verificar se houve vitória
                        playerText.innerHTML = "Player 2" // troca o texto para indicar qual player esta jogando
                        playerText.style.color = "black";
                        corAtual++ //jogado para dentro do loop
                        break
                    }
                }
            }
        }
        if (corAtual === 43) {
            playerText.style.display = "none"
            msgWin.style.color = "black"
            msgWin.style.display = "block"
            msgWin.innerHTML = 'Empate! Fim de Jogo!';
            corAtual = "a"; // para de adicionar o contador das células
            color = null; // para de movimentar a bolinha
        }
    }


})

const acaoDescerBola = (e) => {

    // pega a posição atual da bola que vai ser adicionada
    const posAtual = document.getElementsByClassName(`${pos} posBolasSuperior`)

    if (e.currentTarget.id == pos) {

        for (let i of e.currentTarget.children) {

            if (i.hasChildNodes() == false) {

                //testar se corAtual é ímpar (descer bola preta) ou par (descer bola vermelha)

                if (corAtual % 2 == 0) {

                    createBall(i, bolaPreta.cloneNode()) // adiciona a bola na posição da célula
                    posAtual[0].removeChild(bolaPreta)  // remove a bola antiga para adicionar a nova
                    createBall(posAtual[0], bolaVermelha)
                    posCelulas = Number(i.className[0]) // pega a posição da célula que foi adicionado a bola
                    color = "red";
                    animaDescida(pos, posCelulas)
                    testAllDirections(pos, posCelulas) //aciona a função para verificar se houve vitória
                    playerText.innerHTML = "Player 1" // troca o texto para indicar qual player esta jogando
                    playerText.style.color = "red";
                    corAtual++ //jogado para dentro do loop 
                    break

                } else if (corAtual % 2 == 1) {

                    createBall(i, bolaVermelha.cloneNode()) // adiciona a bola na posição da célula
                    posAtual[0].removeChild(bolaVermelha) // remove a bola antiga para adicionar a nova
                    createBall(posAtual[0], bolaPreta)
                    posCelulas = Number(i.className[0]) // pega a posição da célula que foi adicionado a bola
                    color = "black";
                    animaDescida(pos, posCelulas)
                    testAllDirections(pos, posCelulas) //aciona a função para verificar se houve vitória
                    playerText.innerHTML = "Player 2" // troca o texto para indicar qual player esta jogando
                    playerText.style.color = "black";
                    corAtual++ //jogado para dentro do loop
                    break
                }
            }
        }
    }

    //Se todas as celulas forem preenchidas, gera empate

    if (corAtual === 43) {
        playerText.style.display = "none"
        msgWin.style.color = "black"
        msgWin.style.display = "block"
        msgWin.innerHTML = 'Empate! Fim de Jogo!';
        corAtual = "a"; // para de adicionar o contador das células
        color = null; // para de movimentar a bolinha
    }

}

// Função para adicionar a bola a célula do quadro
const descerBola = (div) => { div.addEventListener('click', acaoDescerBola) }

containerTabela.childNodes.forEach(descerBola) //loopar todas as colunas adicionando event listener

// Função para resetar o jogo
const resetFunction = () => {
    msgWin.style.display = "none"
    playerText.style.display = "block"
    playerText.innerHTML = "Player 1"
    playerText.style.color = "red";
    color = "red";
    for (let i = 1; i < 8; i++) {
        const getLines = document.getElementById(i)
        for (let k of getLines.childNodes) {
            k.innerText = ''
        }
    }
    const posInicial = document.getElementById('posBolas')
    for (let k of posInicial.childNodes) {
        k.innerText = ''
    }
    bolaVermelha = document.createElement('div')
    bolaVermelha.className = 'bolaVermelha'
    posInicial.firstChild.appendChild(bolaVermelha)
    pos = 1
    corAtual = 1
    color = 'red'
}
getButton.addEventListener('click', resetFunction)
