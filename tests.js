// Função para testar a vitória
const testVictory = (contadorVitoria) => {

    // vitória bola vermelha
    if (contadorVitoria.search(/r{4}/g) > -1) {
        corAtual = "a" // para de adicionar o contador das células
        color = null // para de movimentar a bolinha
        playerText.style.display = "none" // esconde texto do player
        msgWin.style.color = "red";
        msgWin.innerHTML = "Victory Red!" // mostra msg de vitória
        msgWin.style.display = "block"

    }  // vitória bola preta
    else if (contadorVitoria.search(/b{4}/g) > -1) {
        corAtual = "a"; // para de adicionar o contador das células
        color = null // para de movimentar a bolinha
        playerText.style.display = "none" // esconde texto do player
        msgWin.style.color = "black";
        msgWin.innerHTML = "Victory Black!" // mostra msg de vitória
        msgWin.style.display = "block"
    } else {
        //console.log('Nada ainda!')
    }
}

const varredura = (listNodes) => {
    let victoryCount = ''
    for (let i of listNodes) {
        if (i.hasChildNodes() == true) {
            if (i.firstChild.className == 'bolaVermelha') {
                victoryCount += 'r'
            } else {
                victoryCount += 'b'
            }
        } else {
            victoryCount += 'n'
        }
        let rCount = victoryCount.search(/r{4}/g)
        let bCount = victoryCount.search(/b{4}/g)
        if (rCount > -1) {
            for (let k = rCount; k < rCount + 4; k++) {
                //Incluir animação para bolas vermelhas na horizontal ou vertical. 
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                listNodes[k].firstChild.className = 'bolaNovaVermelha'
            }
        } else if (bCount > -1) {
            for (let k = bCount; k < bCount + 4; k++) {
                //Incluir animação para bolas pretas na horizontal ou vertical. 
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                listNodes[k].firstChild.className = 'bolaNovaPreta'
            }
        }
    }
    return victoryCount
}

// Função para testar a vitória na vertical
const testVictoryVertical = (columnId) => {
    const coluna = document.getElementById(columnId)
    const percorrerColuna = coluna.childNodes
    let victoryCount = varredura(percorrerColuna)
    testVictory(victoryCount)
}

// Função para testar a vitória na horizontal
const testVictoryHorizontal = (linha) => {
    const linhaInteira = document.getElementsByClassName(`${linha} celulas`)
    let victoryCount = varredura(linhaInteira)
    testVictory(victoryCount)
}

// Pegar a posição minima de colunas e linhas da esquerda para direita
posMinimaEsq = (coluna, linha) => {
    while (coluna > 1 && linha > 1) {
        coluna -= 1
        linha -= 1
    }
    return [coluna, linha]
}

// Pegar a posição maxima de colunas e linhas da esquerda para direita
posMaximaEsq = (coluna, linha) => {
    while (coluna < 7 && linha < 6) {
        coluna += 1
        linha += 1
    }
    return [coluna, linha]
}

// Função para testar a vitória na diagonal da esquerda para direita
const testVictoryDiagonalEsq = (coluna, linha) => {
    let victoryCount = ''
    let idInicial = posMinimaEsq(coluna, linha)[0]
    let classInicial = posMinimaEsq(coluna, linha)[1]
    let idFinal = posMaximaEsq(coluna, linha)[0]
    let classFinal = posMaximaEsq(coluna, linha)[1]
    let idAtual = idInicial
    let classAtual = classInicial
    while (idAtual <= idFinal && classAtual <= classFinal) {
        let cellAtual = document.getElementById(idAtual).childNodes[classAtual - 1]
        if (cellAtual.hasChildNodes() == true) {
            if (cellAtual.firstChild.className == 'bolaVermelha') {
                victoryCount += 'r'
            } else {
                victoryCount += 'b'
            }
        } else {
            victoryCount += 'n'
        }
        let rCount = victoryCount.search(/r{4}\b/g)
        let bCount = victoryCount.search(/b{4}\b/g)
        if (rCount > -1) {
            let classTest = classAtual - 3
            let idTest = idAtual - 3
            console.log(idAtual)
            for (k = idTest; k <= idAtual; k++) {
                //Incluir animação para bolas vermelhas na diagonal esquerda para direita
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                let currElement = document.getElementById(k).childNodes[classTest - 1].firstChild
                console.log(currElement)
                currElement.className = 'bolaNovaVermelha'
                classTest++
            }
        } else if (bCount > -1) {
            let classTest = classAtual - 3
            let idTest = idAtual - 3
            for (k = idTest; k <= idAtual; k++) {
                //Incluir animação para bolas pretas na diagonal esquerda para direita
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                let currElement = document.getElementById(k).childNodes[classTest - 1].firstChild
                currElement.className = 'bolaNovaPreta'
                classTest++
            }
        }
        testVictory(victoryCount)
        idAtual++
        classAtual++
    }
}

// Pegar a posição minima de colunas e linhas da direita para esquerda
const posMinimaDir = (coluna, linha) => {
    while (coluna > 1 && linha < 6) {
        coluna -= 1
        linha += 1
    }
    return [coluna, linha]
}

// Pegar a posição maxima de colunas e linhas da direita para esquerda
const posMaximaDir = (coluna, linha) => {
    while (coluna < 7 && linha > 1) {
        linha -= 1
        coluna += 1
    }
    return [coluna, linha]
}

// Função para testar a vitória na diagonal da direita para esquerda
const testVictoryDiagonalDir = (coluna, linha) => {

    let victoryCountDir = ''
    let idInicialDir = posMinimaDir(coluna, linha)[0]
    let classInicialDir = posMinimaDir(coluna, linha)[1]
    let idFinalDir = posMaximaDir(coluna, linha)[0]
    let classFinalDir = posMaximaDir(coluna, linha)[1]
    let idAtualDir = idInicialDir
    let classAtualDir = classInicialDir
    while (idAtualDir <= idFinalDir && classAtualDir >= classFinalDir) {
        let cellAtualDir = document.getElementById(idAtualDir).childNodes[classAtualDir - 1]
        if (cellAtualDir.hasChildNodes() == true) {
            if (cellAtualDir.firstChild.className == 'bolaVermelha') {
                victoryCountDir += 'r'
            } else {
                victoryCountDir += 'b'
            }
        } else {
            victoryCountDir += 'n'
        }
        let rCount = victoryCountDir.search(/r{4}\b/g)
        let bCount = victoryCountDir.search(/b{4}\b/g)
        if (rCount > -1) {
            let classTest = classAtualDir + 3
            let idTest = idAtualDir - 3
            console.log(idAtualDir)
            for (k = idTest; k <= idAtualDir; k++) {
                //Incluir animação para bolas vermelhas na diagonal direita para esquerda
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                let currElement = document.getElementById(k).childNodes[classTest - 1].firstChild
                console.log(currElement)
                currElement.className = 'bolaNovaVermelha'
                classTest--
            }
        } else if (bCount > -1) {
            let classTest = classAtualDir + 3
            let idTest = idAtualDir - 3
            for (k = idTest; k <= idAtualDir; k++) {
                //Incluir animação para bolas pretas na diagonal direita para esquerda
                //listNodes[k] é o quadrado que contém as bolas
                //listNodes[k].firstChild é a bola
                let currElement = document.getElementById(k).childNodes[classTest - 1].firstChild
                currElement.className = 'bolaNovaPreta'
                classTest--
            }
        }
        testVictory(victoryCountDir)
        idAtualDir++
        classAtualDir--
    }

}

// Função para testar a vitória em todas as direções
const testAllDirections = (coluna, linha) => {
    testVictoryHorizontal(linha)
    testVictoryVertical(coluna)
    testVictoryDiagonalDir(coluna, linha)
    testVictoryDiagonalEsq(coluna, linha)
}
